import * as Phaser from "phaser";
import "./style.css";

// Constants
const cspMeta = document.createElement("meta");
const mainSection = document.createElement("section");
const mainSectionHeading = document.createElement("h1");
const mainSectionSubHeadingText = document.createElement("h2");
const mainSectionStorageText = document.createElement("p");
const mainSectionPhaserText = document.createElement("p");

// Document Head
cspMeta.httpEquiv = "Content-Security-Policy";
cspMeta.content = "script-src 'self' 'unsafe-inline';";
document.head.appendChild(cspMeta);

// Document Body
document.body.appendChild(mainSection);
mainSection.appendChild(mainSectionHeading);
mainSection.appendChild(mainSectionSubHeadingText);
mainSection.appendChild(mainSectionStorageText);
mainSection.appendChild(mainSectionPhaserText);
mainSectionHeading.appendChild(new Text("Phaser-App"));
mainSectionSubHeadingText.appendChild(new Text("> index.ts"));

/**
 * Formats Bytes in a readable manner
 * @param bytes The number to format
 * @param decimals The number of decimals to round to
 * @returns A string containing a number rounded to the number of decimals provided, and
 * the most appropropriate size
 */
function formatBytes(bytes: number, decimals = 2) {
  if (bytes === 0) return "0 Bytes";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

// Check for available storage on the device and show estimated usage
if (navigator && "storage" in navigator) {
  navigator.storage.estimate().then((value) =>
    mainSectionStorageText.appendChild(
      new Text(
        `→ Storage estimate is \
          ${formatBytes(value.usage)} out of \
          ${formatBytes(value.quota)}`
      )
    )
  );
} else {
  mainSectionStorageText.appendChild(new Text(`→ Can't estimate storage`));
}

if (Phaser && "VERSION" in Phaser) {
  mainSectionPhaserText.appendChild(
    new Text(`→ Phaser is at version ${Phaser.VERSION}`)
  );
}
